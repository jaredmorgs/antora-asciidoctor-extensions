= Tabs blocks

The _tabs-block_ extension lets you add a tabbed interface for content such as code languages or commands for different operating system variants.

By presenting content like this, you can condense the vertical space in your procedures and provide readers with a better user experience in your installation documentation.

== Install the extension

// TODO: This section needs refinement and is just a placeholder until I can get more specific instructions about properly adding the extension.

You can enable this extension by adding the `behavior.js` and `extension.js` files into the `/scripts` directory.

You can paste the contents of the `styles.css` file into your existing CSS structure.
Alternatively, you can create a discrete `tabs-block.css` file that extends your default CSS.

== Create tabbed blocks in your documentation

The tabs-block extension uses description lists to create the tabbed structure.
You enclose the description lists inside a special example block with the block name `[tabs]`.

The example block uses one extra `=` (a total of five equals symbols).
The extra length avoids conflicts with any nested example blocks that appear inside the tabbed block.

[source,adoc]
----
[tabs]
=====

=====
----

Each tab is modeled as a description list.
The description list term becomes the tab title.
The term's contents form the body content of each tab.


.Example tabbed block
[source,adoc]
----
[tabs]
=====
First tab:: Plain text content of the first tab. <1>

Second tab::
+ <2>
--
Compound content of the second tab.

* Item in a list in the tab
* Another item in the tab.
--

Third tab::
Code syntax example with source highlighting.

+ <3>
----
rm -rf
----
=====
----
<1> The tab title is the description list term "First tab" and the content is a single sentence.
<2> The "Second tab" description list term's content is wrapped in an open block element.
You can use any mark-up supported by description lists to attach content to your tabs.
<3> The command-line example is attached to the description list term using the `+`.
The introductory sentence opens the description list content.
